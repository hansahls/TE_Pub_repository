---
title: "Experiment 2 - SHK-1 Cells New Candidates TE Analysis"
author: "Hanna Sahlström"
date: "10/6/2021"
output: html_document
---

```{r}
r = getOption("repos")
r["CRAN"] = "http://cran.us.r-project.org"
options(repos = r)
```


###Loading in needed packages.
```{r setup, include=FALSE}
knitr::opts_chunk$set(error = TRUE, echo = TRUE)

library(ggallin)
library("rstudioapi") 
library(tidyverse)
library("ggpubr")
library("ggfortify")
library(multcomp)
library(dplyr)

getwd()
```

##Fold Change Analysis
###Loading in the data and creating function for pre-processing. 
```{r}
fire = "../Data/Luciferase_Assay/fLuc_NEW_E2.csv"
ren = "../Data/Luciferase_Assay/rHluc_NEW_E2.csv"


crunsh_data <- function(input){
    dat <- read_delim(input, col_names = F, delim = ';') # this converts to dbl at once
    colnames(dat) <- c("Sample", paste0("rep", 1:(ncol(dat)-1)))  #Adds headings to data
 
    if(ncol(dat)<3){ #cancels function if there are less than 3 replications
      print("ERROR: Input dataframe has less than two trials. Must have at least two trials to compute statistics.")
      return()
      }
     
    dat <- dat[-1,]
    return(dat)
}

fire.data <- crunsh_data(fire)
ren.data <- crunsh_data(ren)
```

###Calculating the Fold Change: Normalizing the firefly RLUs by dividing by the renilla RLUs, and then calculting the mean of the tree wells. 
```{r}
NormalizedData <- cbind(fire.data[1],(fire.data[-1]/ren.data[-1])) # dividing firefly by renilla, to control for internal variables from well-to-well, in other words calculating fold change
NormalizedData[,-1] <- NormalizedData[,-1]/rowMeans(NormalizedData[1,-1]) #fold change relative to sv40 

NormalizedData$fratio <- rowMeans(NormalizedData[,-1]) #calculates the mean of each sample
#NormalizedData$std <- NormalizedData %>% select(starts_with('rep')) %>% apply(., 1, sd)  #calculates the standard deviation of each sample

NormalizedData$std <- NormalizedData[-1] %>% apply(., 1, sd)  #calculates the standard deviation of each sample

NormalizedData
```

###Plotting the fold changes as a bar graph. 
#### This section is not repeatable. Need to categorize samples manually in line 63, and change ordering manually in line 66.
```{r}
NormalizedData$Category <- c("Negative Control", "Positive Control", "TE", "TE", "TE", "TE", "TE", "TE") #decides category of each sample manually

NormalizedData$Sample <- factor(NormalizedData$Sample,   # Change ordering manually
      levels = c("sv40", "CMV", "Ele0377", "Ele0307", "Ele1054", "Ele0849", "Ele242", "Ele0086"))

ggplot(data = NormalizedData, aes(x = Sample, y = fratio, fill=Category)) +
  geom_col(width=0.8) +
  geom_errorbar(aes(ymin=fratio-std, ymax=fratio+std), size=.3, width=.4) + 
  geom_text(aes(label = round(fratio, 1)), vjust=-0.3, size=3.7) + #places y labels on top of bars
  theme_minimal() + #removes gray background
  geom_hline(aes(yintercept=1), colour="#990000", linetype="dashed") + #adds dashed line across bar graph at specified point
  scale_fill_manual(values = c("Negative Control" = "#F75D59",
                               "Positive Control" = "#4CC552",
                               "TE" = "#357EC7")) + #choose colors for category
  theme(text = element_text(size = 13)) + #select text size
  scale_y_continuous(trans = pseudolog10_trans) + #log transforms y axis
  labs(x="Samples", y = "Fold change relative to sv40 promoter") #labels for y and x axis

ggsave("../Data/Output/Plots/E2_NEW_Barplot.pdf", width = 10, height = 7)
```
Description: Bar plot showing the fold change of each test vector relative to the sv40 promoter in the pGL3 vector, in SHK-1 cells. The red bars represent the negative controls, and the green bars represent the positive controls. The blue bars are the test samples, in this case, whole TEs. The dotted line represents the fold change of the sv40 promoter. The sv40 promoter is present in all samples, and is therefore causing the basal expression level of the firefly luciferase protein. The CMV appears lower than the elovl5b and sv40 promoter, and the TEs have higher fold changes than the CMV. This indicates that something may be wrong with this experiment, with a CMV so low. 


##Further Statistical Analysis
### Reorganizing the Data 
```{r}
Datastat <- NormalizedData[,1:4]
Datastat <- Datastat %>% pivot_longer(!Sample, names_to = NULL, values_to = 'RLU')
Datastat
```

##ANOVA Model
```{r}
M1 <- aov(log(RLU) ~ Sample, data = Datastat)
summary(M1)
capture.output(summary(M1), file = '../Data/Output/Statistical_Analysis/ANOVA_NEW_E2.csv')
```
p-value shows no significant variation between samples. 

##Post-hoc Tukey Multiple Comparison of Means Analysis.
```{r}
TUKEY <- TukeyHSD(M1)
TUKEY
TUKEY_CSV <- as.data.frame(TUKEY[1:1])
TUKEY_CSV
write.csv(TUKEY_CSV, '../Data/Output/Statistical_Analysis/Tukey_NEW_E2.csv')
#capture.output(TukeyHSD(M1), file = '../Output_data/Statistical_Analysis/E3_Tukey.csv')
```
The Tukey test does not display any significant p-values. This is interpreted as a big indicator that something has gone wrong in the experimental setup of this experiment. 

### Plotting Residual Plots to check model assumptions are met. 
```{r}
Residual <- autoplot(M1) + theme_classic()
Residual
ggsave("../Data/Output/Plots/E2_Residual_plots.pdf", width = 10, height = 7) #still issues with just getting 1 of the 4 plots
```


##Creating a boxplot. 
###Log transformation is carried out to make data fit the assumptions made for a normally distributed dataset. Categorization here is manual as well. 
```{r}
Datastat$log_RLU <- log(Datastat$RLU) #log transform RLU values for normal distribution assumption of statistical tests

Category <- c("Negative Control", "Negative Control", "Negative Control", 
              "Positive Control", "Positive Control", "Positive Control", 
              "TE", "TE", "TE", 
              "TE", "TE", "TE", 
              "TE", "TE", "TE", 
              "TE", "TE", "TE",
              "TE", "TE", "TE",
              "TE", "TE", "TE") #Manually creates a vector labelling the positive and negative and test samples

Datastat$Category <- Category #Inserts category vector into datastat for use when plotting


#Extract p-value from ANOVA table as character string.
ANOVA_table <- read_delim("../Data/Output/Statistical_Analysis/ANOVA_NEW_E2.csv", delim = ' ')
p_value <- ANOVA_table[1, "F"]
p_value <- as.character(p_value)
p_value

#Create character string for compact letter display to paste into plot. 
tukeycld <- glht(M1, linfct=mcp(Sample="Tukey"))
CLD <- cld(tukeycld)
letters <- CLD$mcletters$Letters #extracts letters from cld list for pasting into plot.
letters

ggplot(Datastat, aes(Sample, log_RLU, fill = Category)) + geom_boxplot() +
  theme_classic() +
  annotate("text", x = c(1.3, 2), y = 6, label = c("ANOVA:", p_value)) +
  annotate("text", x = c(1,2,3,4,5,6,7, 8), y=5.5, label = letters) + 
  scale_fill_manual(values = c("Negative Control" = "#F75D59",
                               "Positive Control" = "#4CC552",
                               "TE" = "#357EC7"))

ggsave("../Data/Output/Plots/E2_NEW_Boxplot.pdf", width = 10, height = 7)

```
