---
title: "Ridgeplot"
output: 
  html_document: 
    toc: yes
    toc_float: yes
editor_options: 
  chunk_output_type: console
---


```{r}
r = getOption("repos")
r["CRAN"] = "http://cran.us.r-project.org"
options(repos = r)
```

##download packages
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(DT); library(plotly)
library(ape)
library(tidyverse)
library(dplyr)
library(data.table)
library(stringr)
library(ggplot2)
library(ggridges)
```

```{r}
setwd(dirname(getActiveDocumentContext()$path))       
getwd()
```

##Open Salmon Repeat File and Bigblast file
```{r}
TEs <- read.gff('../Data/FASTA/Salmon_Repeat_170415.gff3', na.strings = c(".", "?"), GFF3 = TRUE)

Bigblast <- fread('../Data/Text_files/bigblast.txt', header=FALSE)
head(Bigblast)

subset(Bigblast, grepl('3637503|1695746|3071004|376302|376303|1106838|1106839|376305|622966|1760438|3546399|1760485', Bigblast$V2))

head(Specials)
```


##remove self-matches from the Bigblast file
### You cannot do this based on the percentage match, because there could be two identical TEs in two different locations. Instead, you have to do it based on the names of the TEs. Also we remove Bigblast to save processing power. 
```{r}
BBdiff <- Bigblast[!(Bigblast$V1==V2)]
rm(Bigblast)
```

##delete columns that you do not need, and split columns in TEs based on the semicolon
```{r}
BBdiff <- BBdiff[,1:3]
BBdiff$IDRep <- gsub(";.*","",BBdiff$V1)
BBdiff <- BBdiff[, 3:4]

TEs <- TEs$attributes
TEs <- stringr::str_split_fixed(TEs, ";", 3)
```

#merge the two datasets based on the id column
### first change the TE file from a matrix to a data frame. Then add a sequence ID column in the TE file so that you can sort the columns back to their original order after merging. Then merge TEs with BBdiff, and arrange according to the ID column to get them back in order. 
```{r}
TEs <- as.data.frame(TEs)
TEs$ID <- seq.int(nrow(TEs))

Merge <- merge(TEs, BBdiff, by.x = "V1", by.y = "IDRep", all.x = TRUE)
    Merge <- Merge %>% arrange(ID)
    Merge <- subset (Merge, select = -ID)
h <- head(Merge)
print(h)
rm(BBdiff)
rm(TEs)

```

##My TEs
#ID=Ssa_Repeat_376305;Name=DTT_SsalEle0256;Note=DNA/TcMar-Tc1
#ID=Ssa_Repeat_622966;Name=DTT_SsalEle0180;Note=DNA/TcMar-Tc1
#ID=Ssa_Repeat_1760438;Name=DTT_SsalEle0351;Note=DNA/TcMar-Tc1
##ID=Ssa_Repeat_3546399;Name=DTT_SsalEle0709;Note=DNA/TcMar-Tc1
#ID=Ssa_Repeat_1760485;Name=DTT_SsalEle0401;Note=DNA/TcMar-Tc1

##Now I sort out all the TEs in family SsalEle
```{r}
Merge <- dplyr::filter(Merge, grepl('SsalEle|Om_rnd', V2))
```

##Then we collapse each repeat by averaging the Blast match percentage of each repeat, and extract the specific families of TEs we are interested in. 
```{r}
Merge$ID <- seq.int(nrow(Merge))

Merge <- Merge %>%                                        # Specify data frame
  group_by(Merge[,1:3]) %>%                         # Specify group indicator
  summarise_at(vars(V3.y),              # Specify column
               list(mean)) 

view(Merge)

FinalTE <- dplyr::filter(Merge, grepl('0307|0086|0849|1054|0377|family-242', V2)) 
class(FinalTE)
```

##Now we are ready to make a ridge plot!
```{r}
names(FinalTE) <- c("Repeat", "TE_Family", "Superfamily", "Percentage_Similarity")

FinalTE$Interesting <- grepl('3637503|1695746|3071004|376302|376303|1106838|1106839', FinalTE$Repeat) 


#Checking that TEs actually were extracted
WhatTEs <- dplyr::filter(FinalTE, grepl('TRUE', Interesting))
WhatTEs

#Plotting a Ridgeplot
Ageplot <- ggplot(FinalTE, aes(Percentage_Similarity, TE_Family, group = TE_Family, fill=..x..)) + 
  geom_density_ridges_gradient(jittered_points = TRUE, position = position_points_jitter(height = 0), aes(point_color = Interesting), point_size = 5, point_shape = '|') +
  scale_discrete_manual("point_color", values = c("NA", "black"), guide = "none",) +
  labs(y="TE Families", x = "Percentage Similarity") +
  geom_vline(xintercept=87, linetype = 2, color = "red")


view(Ageplot)
#title = "Age Estimation Based on Sequence Percentage Similarity \nof TE Families in Atlantic #Salmon",

pdf(file = '../Output_data/Plots/Ridgeplot_NEW_TEs')
#A few final touch ups...  
Ageplot + theme_bw() +
  theme(axis.line = element_line(colour = "black"),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank())

dev.off()

```
